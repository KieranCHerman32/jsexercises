// .html() get current content
//   $('selector').html();

// alert(firstH1);

// $('#codeBound').click(function () {
//   var firstH1 = $(this).html();
//   alert(firstH1);
// });
//
// $('#secondHeading').click(function () {
//   var secondH1 = $(this).html();
//   alert(secondH1);
// });
//
// $('#doofTale').click(function () {
//   var doof = $(this).html();
//   alert(doof);
// });
//
// $('#morpheus').click(function () {
//   var bluepill = $(this).html();
//   alert(bluepill);
// });
//
// $('p').css('color', 'red');

//.first
// $(selector).first().css

  // $('li').first().css('color', 'blue').next('li').css('color', 'red');

//EFFECTS

// .hide() .show() .toggle()

// .hide()
$('#marvelHeroesToggle').click(function () {
  $('#marvelHeroes').fadeToggle(500);
});
$('#dcHeroesToggle').click(function () {
  $('#dcHeroes').toggle();
});

// Animations
// .fadeIn() .fadeOut() .fadeToggle() .slideUp() .slideDown() .slideToggle

