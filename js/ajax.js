(function () {
  "use strict";

  // $.ajax( {
  //   URL : "/url",
  //   type : "GET" "POST" "PUT" "DELETE",
  //   data : {
  //     name : "Karen",
  //     location : "San Antonio"
  //   },
  //   dataType : "JSON", "text", "XML",
  //   username : "krivas1234",
  //   password : "12345",
  //   headers : "http header that contains key value",
  //
  // });
  // $.ajax( {
  //   URL : "/data/pressAjax2.json",
  //   type : "GET",
  //   data : {
  //     name : "Karen",
  //     location : "San Antonio"
  //   }
  //   }).done(function (data, status, jqXhr) {
  //   alert('Request successful');
  //   console.log(data);
  //   console.log('The status of my response is ' + status);
  //   console.log(jqXhr)
  // });
  $.ajax("https://swapi.co/api/films", {
  type : "GET",
  data :  {
    search : "A New Hope"
  }
}).done(function (data) {
  console.log(data.results[0].director)
}).fail(function (status) {
console.log(status)
});

})();
