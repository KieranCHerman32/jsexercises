(function () {
  "use strict";
/** Create a new EMPTY array named sports.**/
  const sports = [];
  console.log(sports);
/** Push into this array 5 sports**/
  sports.push('Football', 'Soccer', 'Lacrosse', 'Hockey', 'HEMA');
  console.log(sports);
/** Sort the array.**/
  var sported = sports.sort();
  console.log(sported);
/** Reverse the sort that you did on the array.**/
  var reverseSported = sports.sort(function (a, b) {
    if (a > b) {
      return -1;
    }
    return 0;
  });
  console.log(reverseSported);
/** Push 3 more sports into the array **/
  sports.push('Volleyball', 'Rugby', 'Curling');
  console.log(sports);
/** Locate your favorite sport in the array and Log the index. **/
  console.log(sports[0] + ' (Women\'s Soccer only, though. The men\'s league is a joke.)');
  console.log(sports[3] + ' (I used to do a form of LARP that removed the RP. We called it Mock Combat. HEMA is as close as it gets to that.)');
/** Remove the 4th element in the array **/
  sports.splice(3,1);
  console.log(sports);
/** Remove the first sport in the array **/
  sports.shift();
  console.log(sports);
/** Turn your array into a string  **/
  sports.join();
  console.log(sports.join());
  // const sportString = sports.toString();
  // console.log(sportString);
})();
