// Create a new javascript file name 'es6-promises-drill',
//   copy the following code and paste it in your javascript file,
//   complete each task.

const name = 'Chula';
const age = 14;

// REWRITE THE FOLLOWING USING TEMPLATE STRINGS
// console.log("My dog is named " + name + ' and she is ' + age + ' years old.');

console.log(`My dog is named ${name} and she is ${age} years old.`);
console.log(`My dog is named ${name}, and she is ${age} years old.`);

// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function addTen(num1) {
//   return num1 + 10;
// }

const addTen = (num) => num + 10;
const addTen2 = (num) => num +10;
console.log(addTen(5));
console.log(addTen2(4));

// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function minusFive(num1) {
//   return num1 - 5;
// }

const minus5 = (num) => num - 5;
const minus5Again = (num) => num -5;
console.log(minus5(5));
console.log(minus5Again(10));

// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function multiplyByTwo(num1) {
//   return num1 * 2;
// }

const mult2 = (num) => num * 2;
const mult2a = (num) => num * 2;
console.log(mult2(2));
console.log(mult2a(4));

// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// const greetings = function (name) {
//   return "Hello, " + name + ' how are you?';
// };

const greeting = (name) => `Hello, ${name}! How are you?`;
const greeting2 = (name) => `Hello, ${name}! How are you?`;
console.log(greeting('KC'));
console.log(greeting2('Bro'));

// REWRITE THE FOLLOWING IN AN ARROW FUNCTION
// function haveWeMet(name) {
//   if (name === 'Stephen') {
//     return name + "Nice to see you again";
//   } else {
//     return "Nice to meet you"
//   }
// }

const haveWeMet = (name) => {
  if (name === 'Stephen')
    return `Nice to see you again, ${name}!`;
    return `Nice to meet you, ${name}.`
};
const doIKnowYou = (name) => {
  if (name === 'Adam')
    return `Nice to see you again, ${name}!`;
  return `Nice to meet you, ${name}.`
};

console.log(haveWeMet('Stephen'));
console.log(haveWeMet('Joe'));
console.log(doIKnowYou('Adam'));
console.log(doIKnowYou('Miguel'));

// REWRITE THE FOLLOWING IN AN OBJECT DESTRUCTURING
// var car = {make: 'Ford', model: 'Mustang'};
// var make = car.make;
// var model = car.model;

const car = {make: 'Ford', model: 'Mustang'};
const {make, model} = car;
console.log(car);
console.log(make);
console.log(model);

// REWRITE THE FOLLOWING IN AN OBJECT DESTRUCTURING
// var luke = { occupation: 'jedi', father: 'anakin'};
// var occupation = luke.occupation;
// var father = luke.father;

const luke = {occupation: 'Jedi', father: 'Anakin'};
const {occupation, father} = luke;
console.log(luke);
console.log(occupation);
console.log(father);

// CREATE A PROMISE OBJECT (JUST GET THE STRUCTURE DOWN)

function pleaseHold(num) {
  return new Promise((resolve, reject) => {
    setTimeout(function () {
      resolve('Function Resolved')
    }, num);
  });
}
pleaseHold(4000).then(() => alert(`Thank you for holding. You waited for milliseconds.`));


// CREATE A FUNCTION NAMED 'pleaseHold' THAT ACCEPTS A NUMBER AS A PARAMETER
// AND RETURNS A PROMISE THAT RESOLVES AFTER THE PASSED NUMBER OF MILLISECONDS
// pleaseHold(4000).then(() => alert('Thank you for holding. You waited for 4 seconds.'));

