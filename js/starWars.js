(function () {
  "use strict";

  // TODO: Create an ajax GET request for /data/starWars.json

  var call = $.get("data/starWars.json");
  call.done(function (data) {
    $.each(data, function (index, value) {
      console.log(value);
      var name = value.name;
      var height = value.height;
      var mass = value.mass;
      var birth = value.birth_year;
      var gender = value.gender;

      var char = "<tr>" + "<td>" + name + "</td>" + "<td>" + height + "</td>" + "<td>" + mass + "</td>" + "<td>" + birth + "</td>" + "<td>" + gender + "</td>" + "</tr>";

      $('#insertCharacters').append(char)
    });
  });
  call.fail(function (status, error) {
    console.log(status);
    console.log(error);
  });
  call.always(function () {
    console.log('End');
  })
})();
// TODO: Take the data from starWars.json and append it to the characters table
// HINT: Your data should come back as a JSON object; USE YOUR CONSOLE.LOG() TO INSPECT
// HINT: You will want to target #insertCharacters for your new HTML elements
