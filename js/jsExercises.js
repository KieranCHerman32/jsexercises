(function () {

  "use strict";
//   // Write a function called `count(input)` that takes in a string and returns the number of characters.
//   function count(input) {
//     return input.length;
//   }
//   console.log(count('Fithos Lusec Wecos Vinosec'));
//   // Write a function called `add(a, b)` that returns the sum of a and b
//   function add(a,b) {
//     return a + b;
//   }
//   console.log(add(5,5));
//   // Write a function called `subtract(a, b)` that return the difference between the two inputs.
//   function subtract(a,b) {
//     return a - b;
//   }
//   console.log(subtract(5,10));
//   // Write `multiply(a, b)` function that returns the product
//   function multiply(a,b) {
//     return a * b;
//   }
//   console.log(multiply(5,5));
//   // Write a divide(numerator, denominator) function that returns a divided by b
//   function divide(numerator, denominator) {
//     return numerator / denominator;
//   }
//   console.log(divide(25,5));
//   // Write a remainder(number, divisor) function that returns the remainder left over when dividing `number` by the `divisor`
//   function remainder(number,divisor) {
//     return number % divisor;
//   }
//   console.log(remainder(10,4));
//   // Write the function `square(a)` that takes in a number and returns the number multiplied by itself.
//   function square(a) {
//     // return a * a;
//     return Math.pow(a, 2);
//   }
//   console.log(square(5));
//
//   function return21() {
//     var x = 21;
//     return x
//   }
//   console.log(return21());
//
//   function returnMyName(myName) {
//     myName = prompt('What is your name?')
//     return myName
//   }
//   console.log(returnMyName())
//
//   function addFive(num) {
//     return num + 5;
//   }
//   console.log(addFive(5));
//
//   function sayString(str) {
//     return 'You said \"' + str + '\"';
//   }
//   console.log(sayString('Hello'));
//
//   function sayHola() {
//     console.log('Hola!');
//   }
//   sayHola();

  //Write a function named isOdd().
  function isOdd(x) {
    return (x % 2 !== 0);
  }
  console.log(isOdd(5));

  //Write a function named isEven().
  function isEven(x) {
    return x % 2 === 0;
  }
  console.log(isEven(5));

  //Write a function named isSame(). This will return the input as the same input.
  function isSame(x) {
    return x;
  }
  console.log(isSame('Hello World'));

  //Write a function named isSeven().
  function isSeven(x) {
    console.log(x === 7);
  }
  isSeven(10);

  //Write a function named addTwo().
  function addTwo(x) {
    return x + 2;
  }
  console.log(addTwo(4));

  //Write a function named isMultipleOfTen().
  function isMultipleOfTen(x) {
    console.log(x % 10 === 0);
  }
  isMultipleOfTen(50);

  //Write a function named isMultipleOftwo().
  function isMultipleOfTwo(x) {
    console.log(x % 2 === 0);
  }
  isMultipleOfTwo(50);

  //Write a function named isMultipleOftwoAndFour().
  function isMultipleOfTwoAndFour(x) {
    console.log(x % 2 === 0 && x % 4 === 0);
  }
  isMultipleOfTwoAndFour(20);

  //write a function named isTrue.
  function isTrue() {
  console.log(1 < 2)
  }
  isTrue();

  //Write a function named isFalse.
  function isFalse() {
    console.log(1 > 2);
  }
  isFalse();

  //Write a function named isVowel. Still stuck on this one.

  //Write a function named triple(). This will return an input times 3.
  function triple(x) {
  return x * 3;
  }
  console.log(triple(5));

  //Write a function named QuadNum(). This will return a number times 4.
  function quadNum(x) {
    return x * 4;
  }
  console.log(quadNum(2));
  //Write a function named modulus(). This will return the remainder when a / b.
  function modulus(a,b) {
    console.log(a % b)
  }
  modulus(1, 2);

  //Write a function named degreesToRadians().
  function degreesToRadians(degrees) {
    var pi = Math.PI;
    return degrees * (pi/180);
  }
  console.log(degreesToRadians(1));

  //Write a function named absoluteValue(). This will return absolute value of a number.
  function absoluteValue(x) {
    return Math.abs(x);
  }
  console.log(absoluteValue(-2));

  //Write a function named reverseString(). This will return a string in reverse order.
  function reverseString(str) {
    var splitString = str.split("");
    var reverseArray = splitString.reverse();
    return reverseArray.join("");
  }
  console.log(reverseString('Hello World'));

  var a = 1;
  if (a > 10) {
      console.log('Greater than 10')
  } else if (a > 10 && a < 50) {
      console.log('greater than 10, less than 50')
  } else {
      console.log('a is not 21')
  }

  var highScore = 71;
  var myScore = 1;
  if (myScore > highScore) {
    console.log('New High Score!')
  } else if (myScore > (highScore / 2)) {
    console.log('Try to get the New High Score!')
  } else if (myScore < highScore) {
    console.log('Keep Trying!')
  }

  // var stephen = 80;
  // var karen = 56;
  // var juan = 78;
  // var michelle = 99;
  // 100 - 90 = A
  // 89 - 80 = B
  // 79 - 70 = C
  // 69 - 60 = D
  // < 60 = F

  function rubrik(grade) {
    if (grade >= 60) {
      console.log('You\'ve got a D; Study harder!')
    } else if (grade >= 70) {
      console.log('You\'ve got a C; I know you can do better than that!')
    } else if (grade >= 80) {
      console.log('You\'ve got a B; Keep up the good work!')
    } else if (grade >= 90) {
      console.log('You\'ve got an A; I\'m so proud of you!')
    } else if (grade > 100) {
      console.log('If you didn\'t think I\'d know you cheated, you deserve the grade I\'m giving. You\'ve got an F; You\'ve failed!')
    } else {
      console.log('You\'ve got an F; You\'ve failed!')
    }
  }
  rubrik(1);

  function milDay(hour) {
    if (hour > 21 && hour <= 24 || hour < 5) {
      console.log('Is it "Lights Out" already?')
    } else if (hour > 5 && hour <= 12) {
      console.log('Morning, Sarge.')
    } else if (hour > 12 && hour <= 18) {
      console.log('Afternoon, Sarge.')
    } else if (hour > 18 && hour < 21) {
      console.log('Evening, Sarge.')
    } else if (hour === 21) {
      console.log('Lights Out!')
    } else {
      console.log('You don\'t know military time, do you?')
    }
  }
  milDay(21);

  function seasons(month) {
    month = month.replace(month, month.toLowerCase());
    month = month.replace(month[0], month[0].toUpperCase());
    if (month === 'December' || month === 'January' || month === 'February') {
      console.log('It is Winter in ' + month)
    } else if (month === 'March' || month === 'April' || month === 'May') {
      console.log('It is Spring in ' + month)
    } else if (month === 'June' || month === 'July' || month === 'August') {
      console.log('It is Summer in ' + month)
    } else if (month === 'September' || month === 'October' || month === 'November') {
      console.log('It is Fall in ' + month)
    }
  }
  seasons('OCTOBER')

})();
