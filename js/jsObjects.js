// Objects
// JS Variables are containers for data values //

// var car = 'Corvette'; //

// Objects are variables as well. //
// but Objects can contain many values. //

var car = {
  make: 'Chevy',
  type: 'Corvette',
  model: 'ZR1',
  color: 'red'
};
// Values are written: 'name':'value' pair-set //

// OBJECT LITERAL //
// const person = {
//   firstName: 'Stephen',
//   lastName: 'Guedea',
//   weight: '***',
//   age: '21',
//   ssn: '***-**-****'
// };

//Object Properties//
// firstName = property
// 'Sassy' = property value

// Accessing Properties
// 2 ways

//objectName.propertyName or objectName['propertyName']

// console.log(person.lastName);


//NESTED VALUES - Property value can include arrays.

const vehicle = {
  color: 'purple',
  seatCount: '2',
  wheelCount: '1',
  controlMethod: 'Steering-Wheel'
};

// const person = {
//   type: 'dude',
//   hairCol: 'blond',
//   height: 'smol',
//   muscle: 'swole'
// };

const book = {
  title: 'Storm Front',
  series: 'The Dresden Files',
  protag: 'Harry Dresden',
  antag: 'Victor Sells',
  genre: ['Horror', 'Detective-Noir', 'Fantasy']
};
// console.log(book.genre);

//Create a pet object with the following properties/:
//String name, number age, string species, number valueInDollars
//array nicknames, vaccinations: date, type, description

const pet  = {
  name: 'Rosie',
  age: 7,
  species: 'Pitty',
  valueInDollars: 5500,
  nicknames: [
    'Rose',
    'Rosebud',
    'Rosebutt',
    'Meatball',
    'Bugbear',
    'Fatty',
    'Stupid'
  ],
  vaccinations: [
    // ['Vac 1', 'Date 1', 'Desc. 1'],
    // ['Vac 2', 'Date 2', 'Desc. 2'],
    // ['Vac 3', 'Date 3', 'Desc. 3']
    {
      date: '1/11/1111',
      type: 'Parvo',
      desc: 'Not get Parvo'
    }
  ]
};
// console.log(pet.vaccinations[0].type); // Parvo

// Arrays of Objects

// console.log(user[2].name);

const person = {
  type: 'dude',
  hairCol: 'blond',
  height: 'smol',
  muscle: 'swole',
  greeting: function () {
    // console.log('-From Hell, Jack')
  }
};

// console.log(person.greeting());

// Create an object called movie and include four properties //
// Console.log each property //

const movie = {
  // title: '',
  // genres: [''],
  //
  // releaseDate: '',

  // starring: [
  //   {
  //
  //   }
  // ]

  title: 'Jaws',
  genres: ['Thriller', 'Horror', 'Monster'],

  releaseDate: 'Ju' +
    'ne 20, 1975',
  starring: [
    {
      name: 'Roy Scheider',
      role: 'Chief Martin Brody'
    },
    {
      name: 'Robert Shaw',
      role: 'Quint'
    },
    {
      name: 'Richard Dreyfuss',
      role: 'Matt Hooper'
    },
    {
      name: 'Lorraine Gary',
      role: 'Ellen Brody'
    },
    {
      name: 'Murray Hamilton',
      role: 'Mayor Larry Vaughan'
    }
  ]
};
// Create an array of Objects called Movies that include 4 objects, //
const movies = [
  {
    title: 'Puppet Master',
    release: {
        month: 'October',
        day: 12,
        year: 1989
      },
    review: function myReview() {
      return this.title + ' is a ' + 3 + ' star film.'
    }
  },
  {
    title: 'Halloween',
    release: {
        month: 'October',
        day: 25,
        year: 1978
      },
    review: function myReview() {
      return this.title + ' is a ' + 5 + ' star film.'
    }
  },
  {
    title: 'The Texas Chainsaw Massacre',
    release: {
        month: 'October',
        day: 11,
        year: 1974
      },
    review: function myReview() {
      return this.title + ' is a ' + 4 + ' star film.'
    }
  },
  {
    title: 'The Lost Boys',
    release: {
        month: 'July',
        day: 31,
        year: 1987
      },
    review: function myReview() {
      return this.title + ' is a ' + 5 + ' star film.'
    }
  },
];
// console.log(movies[0-3].review())
// console.log(movies[0].review());
// console.log(movies[1].review());
// console.log(movies[2].review());
// console.log(movies[3].review());

// * BONUS:
//   * Loop through the movies array and output the following information about
// * each book:
//   * - the movie number (use the index of the movie in the array)
// *
// *
// * Example Console Output:
//
//   Movie # 1
// Title: Batman
// another property
// another property
// *      ---
//   Movie # 2
// Title: 1917
// another property
// another property
// *      ---
//   Movie # 3
// Title: Galaxy Quest
// another property
// another property

//'THIS' refers to owner of function

// for (var i = 0; i < movies.length; i++) {
//   console.log('Movie #' + (i + 1) + ': ' + movies[i].title);
//   console.log('' +
//     'Released ' + mov
//   ies[i].release.month + ', ' + movies[i].release.day + ', ' + movies[i].release.year);
//   console.log(movies[i].review());
//   console.log('==========');}

// Constructor Function//

var user = [
  {
    name: 'Bob',
    age: 18,
  },
  {
    name: 'Dude',
    age: '75'
  },
  {
    name: 'Cindy',
    age: '12'
  }
];

function addUser(name, age) {
  return user.push(
    {
      'name': name,
      'age': age
    }
  )
}
// addUser('Kc',24);
// console.log(user);

const albums = [
  {
    artist: 'Michael Jackson',
    title: 'Thrillers',
    released: 1982,
    genre: 'Pop'
  },
  {
    artist: 'AC/DC',
    title: 'Back in Black',
    released: 1980,
    genre: 'Rock'
  },
  {
    artist: 'Pink Floyd',
    title: 'The Dark Side of the Moon',
    released: 1973,
    genre: 'Rock'
  },
  {
    artist: 'Bee Gees',
    title: 'Saturday Night Fever',
    released: 1977,
    genre: 'Disco'
  },
  {
    artist: 'Fleetwood Mac',
    title: 'Rumours',
    released: 1977,
    genre: 'Rock'
  },
  {
    artist: 'Shania Twain',
    title: 'Come On Over',
    released: 1997,
    genre: 'Country'
  },
  {
    artist: 'Michael Jackson',
    title: 'Bad',
    released: 1987,
    genre: 'Pop'},
  {
    artist: 'Led Zeppelin',
    title: 'Led Zeppelin IV',
    released: 1971,
    genre: 'Rock'
  },
  {
    artist: 'The Beatles',
    title: '1',
    released: 2000,
    genre: 'Rock'},
  {
    artist: 'Whitney Houston',
    title: 'Whitney',
    released: 1987,
    genre: 'Pop'
  },
  {
    artist: 'Def Leppard',
    title: 'Hysteria',
    released: 1987,
    genre: 'Rock'
  },
  {
    artist: 'Tupac',
    title: 'All Eyez on Me',
    released: 1996,
    genre: 'Rap'
  },
  {
    artist: 'Eminem',
    title: 'The Marshall Mathers LP',
    released: 2000,
    genre: 'Rap'
  },
  {
    artist: 'Green Day',
    title: 'Dookie',
    released: 1994,
    genre: 'Rock'
  },
  {
    artist: 'Michael Jackson',
    title: 'Dangerous',
    released: 1991,
    genre: 'Pop'
  },
  {
    artist: 'The Notorious B.I.G',
    title: 'Ready to Die',
    released: 1994,
    genre: 'Rap'
  },
  {
    artist: 'Adele',
    title: '21',
    released: 2011,
    genre: 'Pop'
  },
  {
    artist: 'Metallica',
    title: 'Load',
    released: 1996,
    genre: 'Rock'
  },
  {
    artist: 'Prince',
    title: '1999',
    released: 1982,
    genre: 'Pop'
  },
  {
    artist: 'Lady Gaga',
    title: 'Born This Way',
    released: 2011,
    genre: 'Pop'
  }
];
console.log('*** 1. create a for loop function that logs every \'Pop\' album ***');
  for (var i = 0; i < albums.length; i++) {
    if (albums[i].genre === 'Pop') {
      console.log(albums[i].artist + ' - ' + albums[i].title)
    }
  }

console.log('*** 2. create a for each function that logs every \'Rock\' album ***');
  albums.forEach(function (album,index) {
    if (albums[index].genre === 'Rock') {
      console.log(albums[index].title);
    }
  });
console.log('*** 3. create a for each function that logs every album released before 2000 ***');
  albums.forEach(function (album,index) {
    if (albums[index].released < 2000) {
      console.log(albums[index].title);
    }
  });
console.log('*** 4. create a for loop function that logs every album between 1990 - 2020 ***');
  for (var i = 0; i < albums.length; i++) {
    if (albums[i].released >= 1990 && albums[i].released < 2020) {
      console.log(albums[i].artist + ' - ' + albums[i].title)
    }
  }

console.log('*** 4. for loop function that logs every Michael Jackson album ***');
  for (var i = 0; i < albums.length; i++) {
    if (albums[i].artist === 'Michael Jackson') {
      console.log(albums[i].title + ' - ' + albums[i].released)
    }
  }

console.log('*** 5. create a function name \'addAlbum\' that accepts the same parameters from\'albums\' and add it to the array ***');
  function addAlbum(artist, title, released, genre) {
    return albums.push(
      {
        'artist': artist,
        'title': title,
        'released': released,
        'genre': genre
      }
    )
  }
  addAlbum('Zach Callison', 'A Picture Perfect Hollywood Heartbreak', 2018, 'Alternative ');
  console.log(albums);
