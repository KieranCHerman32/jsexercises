//Arrays - Variables that hold multiple values//
const bandsIveSeen = ['Korn', 'Slipknot', 'Twin Method', 'King 810'];
const numbers = [1, 1, 2, 3, 5, 8, 13];

// console.log(bandsIveSeen[3])

bandsIveSeen[4] = 'Blue October';
console.log(bandsIveSeen);

// Better way to add to an Array //
numbers.push(21);
numbers.unshift(0, 0);
console.log(numbers);

bandsIveSeen.unshift('Bullet For My Valentine');
bandsIveSeen.pop();
bandsIveSeen.splice(3, 0, '3OH!3');
console.log(bandsIveSeen);

console.log(bandsIveSeen.length);
//For Loop//
for (var i = 0; i < bandsIveSeen.length; ++i) {
  console.log(bandsIveSeen[i]);
}
for (var x = 0; x < numbers.length; ++x) {
  console.log(numbers[x]);
}
const fruits = ['Tomatoes', 'Peas', 'Avocados', 'Corn'];
console.log(fruits);
for (var y = 0; y < fruits.length; y++) {
  console.log(fruits[y]);
}
console.log("Ya'll thought they were vegetables, didn't you?");

const ages = [33, 12, 20, 16, 5, 54, 21, 44, 61, 13, 15, 45, 25, 64, 32];
console.log(ages.length);
console.log(ages[0]);
console.log(ages[2]);

for (var a = 0; a < ages.length; a++) {
  console.log(ages[a])
}

//forLoop vs forEach//
ages.forEach(function (age) {
  console.log(age);
});

//Filter - Filters things out from array//
//get Age 21+//
const canDrink = ages.filter(function (age) {
  if (age >= 21) {
    return true;
  }
});

console.log(canDrink);

const cannotDrink = ages.filter(function (age) {
  if (age < 21) {
    return true;
  }
});

console.log(cannotDrink);

//Map//
//Creates new arrays from current array//
//get square root of each age//
const ageSquare = ages.map(function (age) {
  return Math.sqrt(age);
});
console.log(ageSquare);

//sort//
//sort ages oldest to youngest//
var sortedAges = ages.sort(function (age1, age2) {
  if (age1 > age2) {
    return -1;
  } else {
    return 1;
  }
});
console.log(sortedAges);

