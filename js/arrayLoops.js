(function () {
  'use strict';

  // var numbers = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0];
  // console.log(numbers.length);
  //
  // for (var i = numbers.length; i >= 0; i--) {
  //   console.log(numbers[i]);
  // }
  //
  // numbers.forEach(function (number) {
  //   console.log(number)
  // });
  /** Create a new array named alphas that holds the names of the people in your class**/
  const alphas = ['Stephen', 'Karen', 'Adam', 'Daniel', 'Joe', 'KC', 'Miguel', 'Mike', 'Yvette'];

  /** Log out each individual name from the array by accessing the index. **/
  console.log(alphas[0]);
  console.log(alphas[1]);
  console.log(alphas[2]);
  console.log(alphas[3]);
  console.log(alphas[4]);
  console.log(alphas[5]);
  console.log(alphas[6]);
  console.log(alphas[7]);
  console.log(alphas[8]);

  /** Using a for loop, Log out every item in the alphas array **/
  for (var i = 0; i < alphas.length; i++) {
    console.log(alphas[i])
  }

  /** Refactor your code using a forEach Function **/
  alphas.forEach(function (alpha) {
    console.log(alpha)
  });

  /** Using a for loop. Remove all instance of 'CodeBound' in the array **/
  var arrays1 = ["fizz", "CodeBound", "buzz", "fizz", "CodeBound", "buzz"];
  for (var x = 0; x < 2; arrays1.shift() + x++) {
    arrays1 = arrays1.sort();
  }
  console.log(arrays1);
  /**Using a forEach loop. Convert them to numbers then sum up all the numbers in the array **/
  var numbers1 = [3, '12', 55, 9, '0', 99];
  var nums = [];
  numbers1.forEach(function (number) {
    nums.push(Number(number));
    var sum = nums.reduce(function (total, current) {
      return total + current
    });
    console.log(sum)
  });
  /**Using a forEach loop Log each element in the array while changing the value to 5 times the original value **/
  var numbers = [7, 10, 22, 87, 30];
  numbers.forEach(function (number) {
    console.log(number * 5)
  });

  // BONUS
  /** Create a function that will take in an integer as an argument and Returns "Even" for even numbers and returns "Odd" for odd numbers **/
  function eOrO(int) {
    if (int % 2 === 0) {
      return int + ' is Even'
    } else if (int % 2 === 1) {
      return int + ' is Odd'
    } else {
      return 'No.'
    }
  }
  console.log(eOrO(6));

  /** Create a function named reverseString that will take in a string and reverse it. There are many ways to do this using JavaScript built-in
   * methods. For this exercise use a loop.*/

  function reverseString(str) {
    var newString = "";
    for (var i = str.length - 1; i >= 0; i--) {
      newString += str[i];
    }
    return newString;
  }
  console.log(reverseString('Fithos Lusec Wecos Vinosec'));

  /**Create a function named converNum that will convert a group of numbers to a reversed array of numbers**
   * 386543 => [3,4,5,6,8,3]*/
  function convertNum(nums) {
    var str = ('' + nums).split('');
    str = Number(str);
    return str
  }
  console.log(convertNum(123456789));

  /** Write a function named phoneNum that accepts an array of 10 integers (between 0 and 9). This will return a string of those numbers in a
   *  phone number format.
   *  phoneNum([7,0,8,9,3,2,0,1,2,3])=> returns "(708) 932-0123"*/

  /** Create a function named removeFirstLast where you remove the first and last character of a string.**/







})();
