(function () {
//BOM//
// Browser Object Model //
// Hierarchy of Objects in Browser //

// Three objects inside BOM //
  // 1 - History Object //
  // -- Browser History //
  // 2 - Window Object //
  // -- Current Browser Window  //
  // 3 - Document Object (DOM) //
  // -- Contains everything within a web page //

  // WINDOW OBJECT //
  // -- Core of the BOM //
  // -- Represents an open Browser Window //
  // -- Represents the Javascript Global Object //

  // prompt();
  // alert();
  // confirm();
  // setTimeout();
  // - Executes code after set time //
  // setInterval();
  // - Executes code on specific intervals //
  // - clearInterval - tells code to stop //

  // function ex() {
  //   setTimeout(function () {
  //     alert('Hi and welcome to CodeBound. The capitalization is very important to corporate branding!');
  //   },7000);
  // }
  // ex();

  // Location Object //
  // can be accessed through the Window and is very helpful //

  // prompt();
  // btw we are going to check your age
  //
  // alert();
  // The world is ending
  //
  // confirm();
  // if you are 18 or older

  // function endTimes() {
  //   setTimeout(function () {
  //     alert('I have been instructed to inform you that the world as you know it it ending! Locate a rapture party near you, Today!')
  //   },500)
  //
  // }
  // endTimes();
  //
  // function ageCheck() {
  //   setTimeout(function () {
  //     confirm('BTW Im totes gt check ur age')
  //   },1000)
  // }
  // ageCheck();
  //
  // function ageCount() {
  //   var ages = setInterval( function () {
  //   confirm('Are you 18 years old or older?')
  //   },1500);
  //   setTimeout(function() {
  //     clearInterval(ages);
  //   },2000)
  // }
  // ageCount();
  //
  // function ageStop() {
  //
  // }
  // ageStop();
  // function ageCount() {
  //   setTimeout(function () {
  //     if (prompt('How old are you? Enter in numbers only!') > 50) {
  //       setInterval(function () {
  //         alert('OK Boomer')
  //       },10000)
  //     } else if (prompt('How old are you? Enter in Numbers only!') > 18) {
  //       setInterval (function () {
  //         alert('Fricken\' Mint!')
  //       },1000);
  //       clearInterval()
  //     }
  //   },5000)
  // }
  // ageCount()

  // DOM  - Represents a web page that can be manipulated through code //
  // Document Object //
  // by targeting: //
  // id //
  // element //
  // or //
  // class //

//   var btnToClick = document.getElementById('btnToClick');
//   // console.log(btnToClick)
// // innerHTML Property
//   var mainHeading = document.getElementById('main-heading');
//   console.log(mainHeading.innerText);
//
//   mainHeading.innerHTML = 'Hello World';
//
//   var searchLink = document.getElementById('search-link');
//   console.log(searchLink.innerHTML);
//
//   console.log(searchLink.hasAttribute('href'));
//   console.log(searchLink.hasAttribute('class'));
//   console.log(searchLink.getAttribute('href'));
//
//   searchLink.setAttribute('class', 'btn default');
//   searchLink.setAttribute('href', 'http://isdmxinjail.com');
//   searchLink.removeAttribute('class');
//   console.log(searchLink);
//
//   // Changing CSS / Styling //
//   // Two ways of doing this //
//   // first way - changing the styling one by one using object notation //
//   // second way - using js to assign a class to the element that is connected to the css styling //
//
//   // First Method //
//   // -- Query Selector //
//   var targetDiv = document.querySelector('div');
//   targetDiv.style.backgroundColor = 'red';
//   targetDiv.style.borderStyle = 'dotted';
//
//   var targetDivB = document.querySelector('div')
//
//   var bodyElement = document.getElementsByTagName('body')[0]
//
//   bodyElement.style.color = '#444';
//   bodyElement.style()

    var lilButton = document.getElementById('btnToClick');

    lilButton.style.backgroundColor = 'hotpink';
    lilButton.style.borderStyle = 'dotted';
    lilButton.innerHTML = 'PUT ME OUT OF MY MISERY';

//Had to copy this code from elsewhere.
//selects all h1 tags in html
//since getElements methods spit out an array, an index value needs to be specified
//instead of specifying styling for each subsequent h1 tag, a for loop is used
//loop iterates through array, changing the style at each index
  var h1s = document.getElementsByTagName("h1");
  for(var i = 0; i < h1s.length; i++) {
    h1s[i].style.color = 'hotpink';
  }

  h1s[0].innerHTML = 'DO IT';
  h1s[1].innerHTML = 'HIT THE BUTTON';
  h1s[2].innerHTML = '';

  var list1 = document.getElementsByTagName('ul')[0];
  list1.style.borderStyle = 'double';
  list1.style.border

  //Event Listeners//
  //elements to be uniquely identified//
  //add an event listener an the behavior that it is listening for//
  //we can tie a function that will trigger when the event happens//


  //removing event listeners
  .removeEventListener()

  //on the same target and with the same parameters you initially used to call the event listener

})();
