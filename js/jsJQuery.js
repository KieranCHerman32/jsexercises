"use strict";

// window.onload = function () {
//   alert('Hello.');
// };


$(document).ready(function () {

  $('*').css('text-align','center');
  $('#head').css('background-color', 'yellow');
  $('#head').css('color', 'blue');
  $('#uno').css('border', '2px double hotpink');

  // JS is written \\
  // alert("DOM is ready to manipulate.");

// var content = $('#wut').html();
//   alert(content)

  // function eventEx(e) {
  //   alert('Hey, s*it happens.')
  // }
  // var ex = document.getElementById('idName')
  // ex.addEventListener('click', eventEx);
  //
  // $('selector').click(handler);

  $('#one').click(function() {
    alert('Stop that!')
  });

  $('#zwei').dblclick(function () {
    alert('I guess that\'s okay...')
  });
  $('#zwei').hover(
    function () {
      $(this).css('background-color', 'black')
    },
    function () {
      $(this).css('background-color', 'purple')
    }
  );
  $('li:odd').css('color', 'hotpink')

  $('#typing').keydown(function () {
    alert('YOU TYPED SOMETHING, BRO!')
  });

  // html
  $('#one').html()
  var newContent = $('#one').html();
  alert(newContent)


});

// alert($('#wut').html());
// jQuery(document).ready();
