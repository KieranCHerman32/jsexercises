function waiting(num) {
  new Promise((resolve, reject) => {
    setTimeout(() => {
      if (num < 1000) {
        resolve('Resolved after ' + num + ' milliseconds.');
      } else if (num > 1000) {
        resolve('Resolved after ' + (num / 1000) + ' seconds');
      } else {
        reject('Rejected after...if I am to be quite honest, you should not be able to meet this condition. How did you do it?');
      }
    }, 1000)
  })
}
const stillWaiting = waiting(250);

console.log(stillWaiting);
stillWaiting.then(data => console.log("!", data));
stillWaiting.catch(error => console.log("!", error));
