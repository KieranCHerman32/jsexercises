(function () {
  "use strict";
  // ES6 //
// ECMAScript || JavsScript 6 //

// EXPONENTIAL OPERATOR //

// old syntax //
// Math.pow(5,2);
// new syntax //
// alert((2**5));

// BLOCK SCOPED VARIABLES //

// without block scope //
// if (true) {
//   var name = 'alpha'
// }
// console.log(name); // Alpha //

// with blocked scope //
// if (true) {
//   let name = 'alpha'
// }
// console.log(name); // undefined //

// CONST - can create block scope variables //
// const className = 'alpha';
// alert('Greetings from ' + ClassName + "!"); // "Greetings from alpha !"
// same as ... //
// alert(`Greetings from ${className} !`);

// ARROW FUNCTIONS //
// const className = function (name) {
//   return "Hello from" + name + "!"
// };
// same as ... //
// const className = (name) => {
//   const greeting = 'Hello from' + name + '!';
//   return greeting}; // this is an arrow function with multiple statements

// DEFAULT FUNCTION PARAMETER VALUES //

// old way //
// function greeting(personName) {
//   if (typeof personName === 'undefined') {
//     personName = 'World';
//   }
//   return 'Whaddup, ' + personName + '?!'
// }
// console.log(greeting()); // "Whaddup World?!"

// ES6 Syntax //
// function greeting(personName = 'Codebound') {
//   return `Whaddup, + ${personName} ?`
// }
// console.log(greeting()); // "Whaddup Codebound?" // By passing in no parameters, parameter defaults to CodeBound.
// "Whaddup Alpha?" by passing in Alpha, the function does not default.

// const greet = (personName = 'CodeBound') => {
//   alert(`Greetings from ${personName} !`)
// };
// console.log(greet());

// OBJECT PROPERTY VARIABLE ASSIGNMENT SHORTHAND //

//old way//
//   const person = {
//     name: name,
//     age: 0
//   };
//   const name = 'CodeBound';
//   const person = {
//     name,
//     age: 0
//   };

// VARIABLE DESTRUCTURING //
//old way//
//   var person = {name: 'CodeBound', age: 1};
//   var name = person.name;
//   var age = person.age;

// es6 syntax //
//   const person = {name: 'CodeBound', age: 1,};
//   const {name, age} = person;
//   console.log(name);
//   console.log(age);

//old way//
//   function personBioFor(person) {
//     var name = person.name;
//     var age = person.age;
//     console.log(name);
//     console.log(age);
//   }
//   const person = {name:'CodeBound', age: 24};
//
// personBioFor(person);

// // es6 syntax //
//   function personBioFor({name, age}) {
//     console.log(name);
//     console.log(age);
//   }
//   const person = {name:'CodeBound', age: 1};
//   personBioFor(person);
//
// DESTRUCTURING ARRAYS //
//   const alphaArr = [1, 2, 3, 4, 5];
//   const [x, y] = alphaArr;
//   console.log(x);
//   console.log(y);
  /*
 * Complete the TODO items below
 */
  const developers = [
    {
      name: 'stephen',
      email: 'stephen@appddictionstudio.com',
      languages: ['html', 'css', 'javascript', 'php']
    },
    {
      name: 'karen',
      email: 'karen@appddictionstudio.com',
      languages: ['java', 'javascript', 'cloud foundry']
    },
    {
      name: 'juan',
      email: 'juan@appddictionstudio.com',
      languages: ['java', 'aws', 'php']
    },
    {
      name: 'casey',
      email: 'casey@codeup.com',
      languages: ['node', 'npm', 'sql']
    },
    {
      name: 'dwight',
      email: 'dwight@codeup.com',
      languages: ['html', 'angular', 'javascript', 'sql']
    }
  ];
// TASK: fill in your name and email and add some programming languages you know
// to the languages array
// TASK: replace the `var` keyword with `const`, then try to reassign a variable
// declared as `const`
  const developer = {name: 'K.C.', email: 'kc.herman32@gmail.com', languages: ['JavaScript', 'HTML']};
  const {name, email, languages} = developer;
// TASK: rewrite the object literal using object property shorthand
  developers.push(developer);
  console.log(developers);
// TODO: replace `var` with `let` in the following variable declarations
  let emails = [];
  let names = [];
// TODO: rewrite the following using arrow functions

  developers.forEach(developer => emails.push(developer.email));
  console.log(emails);

  developers.forEach(developer => names.push(developer.name));
  console.log(names);

// TODO: replace `var` with `let` in the following declaration
  let developerTeam = [];
  developers.forEach(function(developer) {
    // TODO: rewrite the code below to use object destructuring assignment
    //       note that you can also use destructuring assignment in the function
    //       parameter definition
    const {name, email, languages} = developer;
    // TODO: rewrite the assignment below to use template strings
    developerTeam.push(`${name}'s email is ${email}. ${name} knows ${languages.join(', ')}.`);
  });
  console.log(developerTeam);
// TODO: Use `let` for the following variable
  let list = '<ul>';
// TODO: rewrite the following loop to use a for..of loop
  for(const member of developerTeam) {
    // TODO: rewrite the assignment below to use template strings
    list += `<li>${member}</li>`;
  }
  list += '</ul>';

  $('#team').append(list);

  function addNums(x, y) {
  return x + y
  } console.log(addNums(2, 3)); // 5

  function subNums(x, y) {
    return x - y
  } console.log(subNums(2, 3)); // -1

  function multNums(x, y) {
    return x * y
  } console.log(multNums(2, 3)); // 6

  function divNums(x, y) {
    return x / y
  } console.log(divNums(2, 3)); // 0.66

  const addNumsES6 = (x, y) => x + y;
  console.log(addNumsES6(2, 3)); // 5

  const subNumsES6 = (x, y) => x - y;
  console.log(subNumsES6(2, 3)); // -1

  const multNumsES6 = (x, y) => x * y;
  console.log(multNumsES6(2, 3)); // 6

  const divNumsES6 = (x, y) => x / y;
  console.log(divNumsES6(2, 3)); // 0.66

  function barRules (age) {
    if (age >= 55) {
      return `Damn, old man.`
    } else if (age >= 21) {
      return `Pick your poison.`
    } else if (age >= 18) {
      return `Karaoke is mondays only.`
    } else {
    return `Oi get lost.`
    }
  }console.log(barRules(35));

  function barRulesES6 (age = 1) {
    if (age >= 55) {
      return `Damn, old man.`
    } else if (age >= 21) {
      return `Pick your poison.`
    } else if (age >= 18) {
      return `Karaoke is mondays only.`
    } else {
      return 'Oi, fuck off.'
    }
  } console.log(barRulesES6());
})();

