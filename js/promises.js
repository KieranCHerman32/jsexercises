// PROMISES //
// a tool for handling asynchronous events in JavaScript //
// THREE STAGES //
// pending = event has not happened //
// resolved = event happened successfully //
// rejected = event has happened and an error has occurred //

// .then() = accepts callback that will run when the promise is resolved //
// .catch() = accepts callback that will run when the promise is rejected //

// PROMISE OBJECT //
// const thisIsMyPromise = new Promise((resolve, reject) => {
//   if (Math.random() > 0.7) {
//     resolve();
//   } else {
//     reject();
//   }
// });
// thisIsMyPromise.then(() => console.log("Promise Kept"));
// thisIsMyPromise.catch(() => console.log("Promise Broken"));

const thisIsMyPromise = new Promise((resolve, reject) => {
  setTimeout(() => {
    const randNum = Math.random();
    if (randNum > 0.7) {
      resolve();
    } else {
      reject();
    } return console.log(randNum);
  }, 250);
});

console.log(thisIsMyPromise); //pending
thisIsMyPromise.then(() => console.log("Promise Kept"));
thisIsMyPromise.catch(() => console.log("Promise Broken"));

// RESOLVING VALUES //
// resolve and reject can also pass a value
function makeARequest() {
  new Promise((resolve, reject) => {
    setTimeout(() => {
      const randomNum = Math.random();
      if (randomNum % 5 === 0) {
        resolve('Here is the data');
      } else {
        reject('There is no data');
      } return console.log(randomNum);
    }, 1000)
  });
}

const requestingData = makeARequest();

console.log(requestingData);
requestingData.then(data => console.log("Yup", data));
requestingData.catch(error => console.log("Nope", error));
