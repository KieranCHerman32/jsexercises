var listener = function (event) {
  alert('You clicked The Button!');
  alert('Hooray!');
};

document.getElementById('buttonOne').addEventListener('click', listener, false);

var remove = function (event) {
  buttonOne.removeEventListener('click', listener, false);
  console.log('Event Listener Yeeted');
}

var buttonTwo = document.getElementById('buttonTwo');
buttonTwo.addEventListener('click', remove, false);

var storytime = function (event) {
  alert('Particle Man');
  alert('Particle Man');
  alert('Doing the things a particle can');
  alert('What\'s he like?');
  alert('It\'s not important');
  alert('Particle Man');
};

document.getElementById('buttonThree').addEventListener('click', storytime, false);

var remove2 = function (event) {
  buttonThree.removeEventListener('click', storytime, false);
  console.log('Event Listener Yeeted Even Harder')
};

var buttonFour = document.getElementById('buttonFour');
buttonFour.addEventListener('click', remove2, false);

var pinks = document.getElementsByTagName("li");
for(var i = 0; i < pinks.length; i++) {
  if (i % 2 === 0) {
    pinks[i].style.color = 'hotpink';
  }
}
